# sciuridae

## Data

All the code in this repository can be run with data from the Alpine Mammals Updated shared google drive. (This is actually not true quite yet, but it will be in the next day or two.)

## Files

* preprocess/
	
	* 1_mergespecies.R
	* 2_processrasters.R
	* 3_extractdata.R


## preprocess/

__1_mergespecies.R:__ This file takes location data for all the species and merges it together into one file so it can be more easily manipulated later. 

__2_processrasters.R:__ This script merges tiles of digital elevation model rasters and bioclim rasters and merges them into two raster bricks for easy loading later on. It also calculates slope and TRI which are both saved in the DEM raster brick.

__3_extractdata.R:__ (Under Construction) This script loads all the various rasters and shapefiles with variables we want to use for prediction and extracts data from those objects at the location data created in 1_mergespecies.R and random data points that have yet to be located/created.